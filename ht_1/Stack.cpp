#include <iostream>
using namespace std;

class DynArray{
    int RealCount = 0;
    int FullCount;
    int* Array;

    DynArray(int fullCount = 10){
        FullCount = fullCount;
        Array = new int[FullCount];
    }
public:
    void PushBack(int a)
    {
        if (RealCount < FullCount)
        {
            Array[RealCount] = a;
            RealCount++;
        }
        else
        {
            FullCount *= 2;
            int* newArray = new int[FullCount];
            for(int i = 0; i < RealCount; i++)
                newArray[i] = Array[i];
            newArray[RealCount] = a;
            delete[] Array;
            Array = newArray;
            RealCount++;
        }
    }

    int operator[](int index)
    {
        return Array[index];
    }

    void Clear(){
        delete[] Array;
        RealCount = 0;
        FullCount = 0;
    }

    int Capacity()
    {
        return FullCount;
    }

    int Size()
    {
        return RealCount;
    }

    bool Empty()
    {
        return RealCount == 0;
    }
};
