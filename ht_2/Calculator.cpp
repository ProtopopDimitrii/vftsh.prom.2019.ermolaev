#include "Calculator.h"

int Calculator::Calculate(string exp)
{
    int a = ParseDigit(exp);
    int signIndex = GetSignIndex(exp);
    string exp2 = exp.substr(signIndex + 1, exp.length() - signIndex);
    int b = ParseDigit(exp2);
    switch(exp[signIndex]){
        case '+':
            return a + b;
        case '-':
            return a - b;
        case '*':
            return a * b;
        case '/':
            return a / b;
    }
}

int Calculator::GetSignIndex(string exp)
{
    bool isMinusSign = false;
    char sign;
    for(int i = 0; i < exp.length(); i++)
    {
        if(exp[i] == '-' && !isMinusSign)
            isMinusSign = true;
        else if(exp[i] == '-' && isMinusSign)
            return i;
        else if(exp[i] == '+' || exp[i] == '*' || exp[i] == '/')
            return i;
    }
}

int Calculator::ParseDigit(string exp)
{
    int result = 0;
    int sign = 1;
    for(int i = 0; i < exp.length(); i++)
    {
        if(exp[i] >= '0' && exp[i] <= '9')
            result = result * 10 + (exp[i] - '0');
        else if(exp[i] == '-' && result == 0)
            sign = -1;
        else if(exp[i] == '-' && result >= 0)
            break;
        else if(exp[i] == '+' || exp[i] == '/' || exp[i] == '*')
            break;
        else if(exp[i] == ' ')
            continue;
    }
    result *= sign;
    return result;
}