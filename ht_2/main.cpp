#include "Calculator.cpp"
#include <string>

int main(){
    string a;
    getline(cin, a);
    Calculator calc;
    cout << calc.Calculate(a);
    return 0;
}