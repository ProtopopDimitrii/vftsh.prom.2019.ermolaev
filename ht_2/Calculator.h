#ifndef UNTITLED4_CALCULATOR_H
#define UNTITLED_CALCULATOR_H
#include <iostream>
using namespace std;

class Calculator{
public:
    int ParseDigit(string exp);
    int Calculate(string exp);
    int GetSignIndex(string exp);
};

#endif